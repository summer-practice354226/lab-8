﻿#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

struct Address {
    string postcode;
    string country;
    string region;
    string city;
    string street;
    int house;
    int apartment;
};

struct CarOwner {
    string lastName;
    string firstName;
    string patronymic;
    string phoneNumber;
    Address address;
    string carBrand;
    string carNumber;
    string vehiclePassport;
};

vector<CarOwner> createCarOwnerArray() {
    vector<CarOwner> carOwnerArray;
    char continueInput;

    do {
        CarOwner owner;

        cout << "Enter last name: ";
        getline(cin, owner.lastName);

        cout << "Enter first name: ";
        getline(cin, owner.firstName);

        cout << "Enter patronymic: ";
        getline(cin, owner.patronymic);

        cout << "Enter phone number: ";
        getline(cin, owner.phoneNumber);

        cout << "Enter postcode: ";
        getline(cin, owner.address.postcode);

        cout << "Enter country: ";
        getline(cin, owner.address.country);

        cout << "Enter region: ";
        getline(cin, owner.address.region);

        cout << "Enter city: ";
        getline(cin, owner.address.city);

        cout << "Enter street: ";
        getline(cin, owner.address.street);

        cout << "Enter house number: ";
        cin >> owner.address.house;
        cin.ignore();

        cout << "Enter apartment number: ";
        cin >> owner.address.apartment;
        cin.ignore();

        cout << "Enter car brand: ";
        getline(cin, owner.carBrand);

        cout << "Enter car number: ";
        getline(cin, owner.carNumber);

        cout << "Enter vehicle passport number: ";
        getline(cin, owner.vehiclePassport);

        carOwnerArray.push_back(owner);

        cout << "Do you want to add another car owner? (y/n) ";
        cin >> continueInput;
        cin.ignore();
    } while (continueInput == 'y' || continueInput == 'Y');

    return carOwnerArray;
}

void displayCarOwnerArray(const vector<CarOwner>& carOwnerArray) {
    for (const auto& owner : carOwnerArray) {
        cout << "Last name: " << owner.lastName << endl;
        cout << "First name: " << owner.firstName << endl;
        cout << "Patronymic: " << owner.patronymic << endl;
        cout << "Phone number: " << owner.phoneNumber << endl;
        cout << "Address: " << owner.address.postcode << ", " << owner.address.country << ", " << owner.address.region << ", "  << owner.address.city << ", " << owner.address.street << ", " << owner.address.house << ", " << owner.address.apartment << endl;
        cout << "Car brand: " << owner.carBrand << endl;
        cout << "Car number: " << owner.carNumber << endl;
        cout << "Vehicle passport number: " << owner.vehiclePassport << endl;
        cout << endl;
    }
}

void searchByPhoneNumber(const vector<CarOwner>& carOwnerArray) {
    string searchPhoneNumber;
    cout << "Enter phone number to search: ";
    getline(cin, searchPhoneNumber);

    bool found = false;
    for (const auto& owner : carOwnerArray) {
        if (owner.phoneNumber == searchPhoneNumber) {
            cout << "Last name: " << owner.lastName << endl;
            cout << "First name: " << owner.firstName << endl;
            cout << "Patronymic: " << owner.patronymic << endl;
            cout << "Phone number: " << owner.phoneNumber << endl;
            cout << "Address: " << owner.address.postcode << ", " << owner.address.country << ", " << owner.address.region << ", "  << owner.address.city << ", " << owner.address.street << ", " << owner.address.house << ", " << owner.address.apartment << endl;
            cout << "Car brand: " << owner.carBrand << endl;
            cout << "Car number: " << owner.carNumber << endl;
            cout << "Vehicle passport number: " << owner.vehiclePassport << endl;
            cout << endl;
            found = true;
        }
    }

    if (!found) {
        cout << "No car owner found with the specified phone number." << endl;
    }
}

bool compareByLastName(const CarOwner& a, const CarOwner& b) {
    return a.lastName < b.lastName;
}

void sortByLastName(vector<CarOwner>& carOwnerArray) {
    sort(carOwnerArray.begin(), carOwnerArray.end(), compareByLastName);
}

int main() {
    vector<CarOwner> carOwnerArray = createCarOwnerArray();
    displayCarOwnerArray(carOwnerArray);
    searchByPhoneNumber(carOwnerArray);
    sortByLastName(carOwnerArray);
    cout << "Array sorted by last name:" << endl;
    displayCarOwnerArray(carOwnerArray);
    return 0;
}